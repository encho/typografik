import React from "react";
import styled, { ThemeProvider } from "styled-components";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import { darken, lighten } from "polished";

import theme from "./myTheme";
import Home from "./pages/Home";
import BaseText from "./pages/BaseText/BaseText";
import Typography from "./pages/Typography";
import References from "./pages/References";
import Stylesheet from "./pages/Stylesheet";
import Settings from "./pages/Settings";
import GlobalStyle from "./GlobalStyle";
import useTypography from "./typography/useTypography";

const Layout = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: stretch;

  a {
    text-decoration: none;
    color: var(--colors-primary);
  }

  a:hover {
    color: var(--colors-primary);
  }

  a.active {
    text-decoration: underline;
  }
`;

const SideNav = styled.nav`
  padding: 30px;
  flex-grow: 1;
  flex-basis: 256;
  min-height: 100vh;

  ul {
    padding-left: 0px;
    list-style-type: none;
    margin-block-start: 0;
    margin-block-end: 0;
  }

  a {
    text-decoration: none;
  }
`;

const PageContent = styled.div`
  padding: 30px 60px;
  flex-grow: 99999;
  flex-basis: 0;
  min-width: 480px;
`;

const App: React.FC = () => {
  const typography = useTypography();

  return (
    <Router>
      <ThemeProvider theme={{ ...theme, typography }}>
        <>
          <GlobalStyle />
          <Layout>
            <SideNav>
              <ul>
                <li>
                  <NavLink exact to="/">
                    Home
                  </NavLink>
                </li>
                <li>
                  <NavLink exact to="/typography">
                    Typography
                  </NavLink>
                </li>
                <li>
                  <NavLink exact to="/base-text">
                    Custom Properties
                  </NavLink>
                </li>
                <li>
                  <NavLink exact to="/settings">
                    Settings
                  </NavLink>
                </li>
                <li>
                  <NavLink exact to="/stylesheet">
                    Stylesheet
                  </NavLink>
                </li>
                <li>
                  <NavLink exact to="/references">
                    References
                  </NavLink>
                </li>
              </ul>
            </SideNav>
            <PageContent>
              <Route path="/" exact component={Home} />
              <Route path="/base-text/" component={BaseText} />
              <Route path="/typography/" component={Typography} />
              <Route path="/references/" component={References} />
              <Route path="/stylesheet/" component={Stylesheet} />
              <Route path="/settings/" component={Settings} />
            </PageContent>
          </Layout>
        </>
      </ThemeProvider>
    </Router>
  );
};

export default App;
