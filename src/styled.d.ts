// import original module declarations
import "styled-components";
import { TTypographySpec } from "./typography/typography";

// and extend them!
declare module "styled-components" {
  export interface DefaultTheme {
    borderRadius: string;

    colors: {
      main: string;
      secondary: string;
      background: string;
    };

    typography: Array<TTypographySpec>;
  }
}
