import typography from "./typography";

// Usage
// function App() {
//   const typography = useTypography();
//
//   return (
//     <div>
//       {typography[0].fontSize}
//     </div>
//   );
// }

// Hook
const useTypography = () => {
  // const isClient = typeof window === "object";
  return typography;
};

export default useTypography;
