import { range, zip, flatten, flatMap } from "lodash";
import { scaleLinear } from "d3-scale";

import { TTypographySpec } from "./typography";

const typographicData = (from: TTypographySpec, to: TTypographySpec) => {
  const viewportWidths = range(from.breakpoint, to.breakpoint);

  const fontSizeScale = scaleLinear()
    .domain([from.breakpoint, to.breakpoint])
    .range([from.fontSize, to.fontSize]);

  const lineHeightScale = scaleLinear()
    .domain([from.breakpoint, to.breakpoint])
    .range([from.lineHeight, to.lineHeight]);

  const fontSizes = viewportWidths.map(vw => fontSizeScale(vw));
  const lineHeights = viewportWidths.map(vw => lineHeightScale(vw));

  const pairs = zip(viewportWidths, fontSizes, lineHeights) as Array<
    Array<number>
  >;
  const data = pairs.map(
    ([vw, fontSize, lineHeight, fontSizeRemH1, approxLineHeightH1]) => ({
      vw, // TODO deprecate
      viewportPixels: vw,
      fontSize,
      lineHeight,
      baseLine: fontSize * lineHeight,
      measure: from.measure
    })
  );
  return data;
};

type TSeriesItem = {
  vw: number;
  viewportPixels: number;
  fontSize: number;
  lineHeight: number;
  baseLine: number;
  measure: number;
};

// TODO integrate into useTypographySeries hook??
const getFluidTypographySeries = (
  typography: Array<TTypographySpec>
): Array<TSeriesItem> => {
  const data = flatMap(range(typography.length - 1), i =>
    typographicData(typography[i], typography[i + 1])
  );
  return data;
};

export default getFluidTypographySeries;
