// enum TFontFamily {
//   display = "display",
//   text = "text"
// }

// export type TFontFamilies = { [k in TFontFamily]: string };
//
// export const fontFamilies = {
//   display:
//     "-apple-system, system-ui, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Ubuntu, Arial, sans-serif",
//   text:
//     "-apple-system, system-ui, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Ubuntu, Arial, sans-serif"
// };
//
// enum TFontWeight {
//   light = "light",
//   normal = "normal",
//   bold = "bold"
// }
//
// export type TFontWeights = { [k in TFontWeight]: number };
//
// export const fontWeights = {
//   light: 300,
//   normal: 500,
//   bold: 700
// };

export type TTypographySpec = {
  breakpoint: number; // TODO how to transition to rems?
  fontSize: number; // TODO how to transition to rems?
  measure: number; // not fluid
  lineHeight: number;
  modularScale: number; // not fluid
};

// TODO add (device) names
export const typographySpec0: TTypographySpec = {
  breakpoint: 0,
  fontSize: 16,
  lineHeight: 1.4,
  measure: 30,
  modularScale: 1.1
};

export const typographySpec200: TTypographySpec = {
  breakpoint: 200,
  fontSize: 16,
  lineHeight: 1.4,
  measure: 30,
  modularScale: 1.1
};

export const typographySpec320: TTypographySpec = {
  breakpoint: 320,
  fontSize: 16,
  lineHeight: 1.4,
  measure: 30,
  modularScale: 1.2
};

export const typographySpec500: TTypographySpec = {
  breakpoint: 500,
  fontSize: 18,
  lineHeight: 1.4,
  measure: 30,
  modularScale: 1.2
};

export const typographySpec1000: TTypographySpec = {
  breakpoint: 1000,
  fontSize: 20,
  lineHeight: 1.4,
  measure: 30,
  modularScale: 1.25
};

// export const typographySpec1500: TTypographySpec = {
//   breakpoint: 1500,
//   fontSize: 22,
//   lineHeight: 1.4,
//   measure: 30,
//   modularScale: 1.25
// };

export const typographySpec2500: TTypographySpec = {
  breakpoint: 2500,
  fontSize: 24,
  lineHeight: 1.4,
  measure: 30,
  modularScale: 1.3
};

export const typographySpec3500: TTypographySpec = {
  breakpoint: 3500,
  fontSize: 24,
  lineHeight: 1.4,
  measure: 30,
  modularScale: 1.3
};

const typography = [
  typographySpec0,
  typographySpec320,
  typographySpec500,
  typographySpec1000,
  // typographySpec1500,
  typographySpec2500,
  typographySpec3500
];

export default typography;
