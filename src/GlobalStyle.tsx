import { createGlobalStyle, css } from "styled-components";

import { TTypographySpec } from "./typography/typography";

const round = (number, precision = 2) => +number.toFixed(precision);

const fontSize = (setup: TTypographySpec) => {
  return css`
    --fontSize: ${setup.fontSize + "px"};
  `;
};

const fluidFontSize = (small: TTypographySpec, large: TTypographySpec) => {
  return css`
    --fontSize: calc(
      ${small.fontSize + "px"} + (${large.fontSize} - ${small.fontSize}) *
        (100vw - ${small.breakpoint}px) /
        (${large.breakpoint} - ${small.breakpoint})
    );
  `;
};

const baseLine = (setup: TTypographySpec) => {
  const setupBaseLine = setup.lineHeight * setup.fontSize;
  return css`
    --baseLine: ${setupBaseLine + "px"};
  `;
};

const fluidBaseLine = (small: TTypographySpec, large: TTypographySpec) => {
  const smallBaseLine = round(small.lineHeight * small.fontSize);
  const largeBaseLine = round(large.lineHeight * large.fontSize);
  return css`
    --baseLine: calc(
      ${smallBaseLine + "px"} + (${largeBaseLine} - ${smallBaseLine}) *
        (100vw - ${small.breakpoint}px) /
        (${large.breakpoint} - ${small.breakpoint})
    );
  `;
};

const measure = (setup: TTypographySpec) => {
  return css`
    --measure: calc(${setup.measure} * var(--baseLine));
  `;
};

const modularScaleSizes = (typographySpec: TTypographySpec) => {
  const factor = typographySpec.modularScale;
  const exponents = [-2, -1, 0, 1, 2, 3, 4, 5, 6, 7];
  const sizes = exponents.map(exponent => Math.pow(factor, exponent));
  const sizesInRem = sizes.map(size => `${round(size)}rem`);
  return sizesInRem;
};

const modularScale = (typographySpec: TTypographySpec) => {
  const sizesInRem = modularScaleSizes(typographySpec);

  return css`
    --modularScale__-2: ${sizesInRem[0]};
    --modularScale__-1: ${sizesInRem[1]};
    --modularScale__0: ${sizesInRem[2]};
    --modularScale__1: ${sizesInRem[3]};
    --modularScale__2: ${sizesInRem[4]};
    --modularScale__3: ${sizesInRem[5]};
    --modularScale__4: ${sizesInRem[6]};
    --modularScale__5: ${sizesInRem[7]};
    --modularScale__6: ${sizesInRem[8]};
    --modularScale__7: ${sizesInRem[9]};
  `;
};

const setupTypography = (setup: TTypographySpec) => {
  return css`
    :root {
      ${fontSize(setup)}
      ${baseLine(setup)}
      ${measure(setup)}
      ${modularScale(setup)}
    }
  `;
};

const closeTypography = (close: TTypographySpec) => {
  return css`
    @media only screen and (min-width: ${close.breakpoint}px) {
      :root {
        ${fontSize(close)}
        ${baseLine(close)}
        ${measure(close)}
        ${modularScale(close)}
      }

    }
  `;
};

const fluidTypography = (small: TTypographySpec, large: TTypographySpec) => {
  return css`
    @media only screen and (min-width: ${small.breakpoint}px) and (max-width: ${
    large.breakpoint
  }px) {
      :root {
        ${fluidFontSize(small, large)}
        ${fluidBaseLine(small, large)}

        ${modularScale(small)}
        ${measure(small)}
      }

    }
  `;
};

const GlobalStyle = createGlobalStyle`

  ${props => {
    const { typography } = props.theme;

    // let styles: FlattenSimpleInterpolation[] = [];
    let styles: any[] = [];
    styles.push(setupTypography(typography[0]));
    for (let i = 1; i < typography.length; i++) {
      styles.push(fluidTypography(typography[i - 1], typography[i]));
    }
    styles.push(closeTypography(typography[typography.length - 1]));
    return styles;
  }}

  html {
    font-size: var(--fontSize);
    line-height: var(--baseLine);
  }
`;

export default GlobalStyle;
