// my-theme.ts
// import { DefaultTheme } from "styled-components";
//
// const myTheme: DefaultTheme = {
//   borderRadius: "5px",
//
//   colors: {
//     main: "cyan",
//     secondary: "magenta",
//     background: "#101030"
//   }
// };

const myTheme = {
  borderRadius: "5px",

  colors: {
    main: "cyan",
    secondary: "magenta",
    background: "#101030"
  }
};

export default myTheme;
