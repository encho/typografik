import React from "react";
import { Text } from "@vx/text";

const ChartTitle = ({ children }) => (
  <Text
    textAnchor="middle"
    verticalAnchor="start"
    style={{
      fontSize: "var(--modularScale__1)",
      fill: "var(--colors-chart-title)",
      // dy: "var(--baseLine)",
      fontWeight: "500"
    }}
  >
    {children}
  </Text>
);

export default ChartTitle;
