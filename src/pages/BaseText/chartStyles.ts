const greys = [
  "#ffffff",
  "#dddddd",
  "#cccccc",
  "#aaaaaa",
  "#909090",
  "#888888",
  "#666666",
  "#444444",
  "#303030",
  "#202020",
  "#000000"
];

// https://graf1x.com/shades-of-orange-color-palette/
const aprikot = "#ef820d";
const amber = "#ffbf00";

const dataColors = [aprikot, amber];
// const dataColors = ["#00aadd", "#00ddaa"];

// const greys = ["#ffffff", "#e9e9e5", "#d4d6c8", "#9a9b94", "#52524e"];
const chartStyles = {
  plot: {
    colors: {
      background: greys[9]
    }
  },
  body: {
    colors: {
      background: greys[8]
    }
  },
  title: {
    colors: {
      text: greys[2]
    }
  },
  typography: {
    colors: {
      // line: greys[2],
      line: greys[7],
      circle: greys[1]
      // line: "#f0d000"
      // line: "#f0d000"
    }
  },
  windowWidth: {
    colors: {
      line: greys[1]
    }
  },
  axis: {
    colors: {
      label: greys[4],
      tickLabel: greys[6],
      stroke: greys[6],
      tickStroke: greys[6]
    }
  },
  data: {
    colors: [...dataColors, ...greys]
  }
};

export default chartStyles;
