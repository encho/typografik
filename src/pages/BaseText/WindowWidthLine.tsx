import React from "react";
import { Line } from "@vx/shape";

const WindowWidthLine = props => {
  return (
    <Line
      // from={{ x: windowWidthX, y: padding }}
      // to={{ x: windowWidthX, y: yMax }}
      stroke={"var(--colors-chart-marker)"}
      strokeWidth={2}
      style={{ pointerEvents: "none" }}
      {...props}
    />
  );
};

export default WindowWidthLine;
