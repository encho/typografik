import React, { useRef } from "react";
import useComponentSize from "@rehooks/component-size";
import useTypography from "../../typography/useTypography";
import FontsizeChart from "./FontsizeChart";
import LineheightChart from "./LineheightChart";
import MeasureChart from "./MeasureChart";
import getFluidTypographySeries from "../../typography/getFluidTypographySeries";

const BaseText = () => {
  const ref = useRef(null);
  const size = useComponentSize(ref);
  const typography = useTypography();
  const data = getFluidTypographySeries(typography); // useFluidTypographySeries
  const height = 400;

  return (
    <div>
      <h1>Base Text Variables</h1>
      <p>
        Typografik's CSS provides custom properties relevant for styling fluid
        web typography and design systems. Custom properties relevant for the
        styling of base text are:
      </p>
      <ul>
        <li>Font size var(--fontSize)</li>
        <li>Line height var(--lineHeight)</li>
        <li>Baseline var(--baseline)</li>
        <li>Measure var(--measure)</li>
      </ul>
      <div
        style={{
          width: "800px"
        }}
      >
        <div ref={ref}>
          <FontsizeChart
            typography={typography}
            data={data}
            width={size.width}
            height={height}
          />
          <div
            style={{
              width: "100%",
              marginTop: "1rem"
            }}
          >
            <LineheightChart
              typography={typography}
              data={data}
              width={size.width}
              height={height}
            />
          </div>
          <div
            style={{
              width: "100%",
              marginTop: "1rem"
            }}
          >
            <MeasureChart
              typography={typography}
              data={data}
              width={size.width}
              height={height}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default BaseText;
