import React from "react";
import { extent, max } from "d3-array";
import { LinePath, Line, Circle } from "@vx/shape";
import { scaleLinear, scaleOrdinal } from "@vx/scale";
import { AxisLeft, AxisRight, AxisBottom } from "@vx/axis";
import { LegendOrdinal } from "@vx/legend";
import { Group } from "@vx/group";
import { Text } from "@vx/text";

import useWindowSize from "../../hooks/useWindowSize";
import chartStyle from "./chartStyles";
import ChartTitle from "./ChartTitle";
import WindowWidthLine from "./WindowWidthLine";

const numTicksForWidth = width => {
  if (width <= 300) return 2;
  if (300 < width && width <= 600) return 5;
  if (600 < width && width <= 900) return 7;
  return 10;
};

const MeasureChart = ({ typography, data, width, height }) => {
  const windowSize = useWindowSize();

  if (!data) return null;

  const xSelector = d => d.viewportPixels;
  const ySelector = d => d.measure;

  const padding = 80;
  const xMax = width - padding;
  const yMax = height - padding;

  const xScale = scaleLinear({
    domain: extent(data, xSelector),
    range: [padding, xMax]
  });

  const ordinalColorScale = scaleOrdinal({
    domain: ["var(--measure)"],
    range: chartStyle.data.colors
  });

  const dataMax = max(data, ySelector);
  const dataMin = 1;
  const yScale = scaleLinear({
    domain: [dataMin, (dataMax as any) * 1.2],
    range: [yMax, padding]
  });

  const chartData = data.map(d => ({
    x: xScale(xSelector(d)),
    y: yScale(ySelector(d))
  }));

  const windowWidthX = xScale(windowSize.width);

  return (
    <svg width={width} height={height}>
      <rect
        x={0}
        y={0}
        width={width}
        height={height}
        fill={chartStyle.plot.colors.background}
      />
      <rect
        x={padding}
        y={padding}
        width={width - 2 * padding}
        height={height - 2 * padding}
        fill={chartStyle.body.colors.background}
      />

      <g transform={`translate(${width / 2} ${30})`}>
        <ChartTitle>Measure</ChartTitle>
      </g>

      {typography.map(typographySpec => (
        <Group>
          <Line
            key={`${typographySpec.breakpoint}`} // TODO more unique key
            from={{ x: xScale(typographySpec.breakpoint), y: padding }}
            to={{ x: xScale(typographySpec.breakpoint), y: yMax }}
            stroke={chartStyle.typography.colors.line}
            strokeWidth={1.5}
            style={{ pointerEvents: "none" }}
          />

          <Circle
            // className="dot"
            cx={xScale(typographySpec.breakpoint)}
            // next should be generic already
            cy={yScale(typographySpec.measure)}
            r={5}
            fill={chartStyle.data.colors[0]}
            opacity={0.35}
            // onMouseEnter={event => {
            //   if (tooltipTimeout) clearTimeout(tooltipTimeout);
            //   props.showTooltip({
            //     tooltipLeft: cx,
            //     tooltipTop: cy + 20,
            //     tooltipData: point
            //   });
            // }}
            // onMouseLeave={event => {
            //   tooltipTimeout = setTimeout(() => {
            //     props.hideTooltip();
            //   }, 300);
            // }}
            // onTouchStart={event => {
            //   if (tooltipTimeout) clearTimeout(tooltipTimeout);
            //   props.showTooltip({
            //     tooltipLeft: cx,
            //     tooltipTop: cy - 30,
            //     tooltipData: point
            //   });
            // }}
          />

          <Circle
            // className="dot"
            cx={xScale(typographySpec.breakpoint)}
            // next should be generic
            cy={yScale(typographySpec.measure)}
            r={2}
            fill={chartStyle.data.colors[0]}
            // onMouseEnter={event => {
            //   if (tooltipTimeout) clearTimeout(tooltipTimeout);
            //   props.showTooltip({
            //     tooltipLeft: cx,
            //     tooltipTop: cy + 20,
            //     tooltipData: point
            //   });
            // }}
            // onMouseLeave={event => {
            //   tooltipTimeout = setTimeout(() => {
            //     props.hideTooltip();
            //   }, 300);
            // }}
            // onTouchStart={event => {
            //   if (tooltipTimeout) clearTimeout(tooltipTimeout);
            //   props.showTooltip({
            //     tooltipLeft: cx,
            //     tooltipTop: cy - 30,
            //     tooltipData: point
            //   });
            // }}
          />
        </Group>
      ))}

      <AxisLeft
        scale={yScale}
        top={0}
        left={padding}
        stroke={chartStyle.axis.colors.stroke}
        tickStroke={chartStyle.axis.colors.tickStroke}
        labelProps={{
          fill: chartStyle.axis.colors.label,
          textAnchor: "middle",
          fontSize: 12,
          fontFamily: "Arial",
          dx: "0.75em"
        }}
        tickLabelProps={(value, index) => ({
          fill: chartStyle.axis.colors.tickLabel,
          textAnchor: "end",
          fontSize: 10,
          fontFamily: "Arial",
          dx: "-0.25em",
          dy: "0.25em"
        })}
      />
      <AxisRight
        scale={yScale}
        top={0}
        left={width - padding}
        stroke={chartStyle.axis.colors.stroke}
        tickStroke={chartStyle.axis.colors.tickStroke}
        labelProps={{
          fill: chartStyle.axis.colors.label,
          textAnchor: "middle",
          fontSize: 12,
          fontFamily: "Arial",
          dx: "-0.75em"
        }}
        tickLabelProps={(value, index) => ({
          fill: chartStyle.axis.colors.tickLabel,
          textAnchor: "end",
          fontSize: 10,
          fontFamily: "Arial",
          dx: "1.50em",
          dy: "0.25em"
        })}
      />
      <AxisBottom
        scale={xScale}
        top={yMax}
        label={"Viewport Width in Pixel"}
        numTicks={numTicksForWidth(width)}
        stroke={chartStyle.axis.colors.stroke}
        tickStroke={chartStyle.axis.colors.tickStroke}
        labelProps={{
          fill: chartStyle.axis.colors.label,
          textAnchor: "middle",
          fontSize: 12,
          fontFamily: "Arial",
          dy: "0.5em"
        }}
        tickLabelProps={(value, index) => ({
          fill: chartStyle.axis.colors.tickLabel,
          textAnchor: "middle",
          fontSize: 10,
          fontFamily: "Arial"
        })}
      />
      <LinePath
        data={chartData}
        strokeWidth={2}
        stroke={chartStyle.data.colors[0]}
        x={d => d.x}
        y={d => d.y}
      />

      <WindowWidthLine
        from={{ x: windowWidthX, y: padding }}
        to={{ x: windowWidthX, y: yMax }}
      />

      <LegendOrdinal scale={ordinalColorScale}>
        {labels => {
          const legendPadding = 15;
          // TODO dynamically with css
          return (
            <g
              transform={`translate(${padding + legendPadding} ${padding +
                legendPadding})`}
            >
              {labels.map((label, i) => {
                return (
                  <g
                    transform={`translate(${0} ${20 * i})`}
                    key={`legend-quantile-${i}`}
                    // onClick={event => {
                    //   alert(`clicked: ${JSON.stringify(label)}`);
                    // }}
                  >
                    <Text
                      verticalAnchor="start"
                      fill={label.value}
                      style={{ fontSize: "0.75rem" }}
                    >
                      {label.text}
                    </Text>
                  </g>
                );
              })}
            </g>
          );
        }}
      </LegendOrdinal>
    </svg>
  );
};

export default MeasureChart;
