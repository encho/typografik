import React from "react";

const References = () => {
  return (
    <div>
      <h1>References</h1>
      <ul>
        <li>
          <a
            href="https://ia.net/topics/the-web-is-all-about-typography-period"
            target="_blank"
            rel="noopener noreferrer"
          >
            Web Design is 95% Typography
          </a>
        </li>

        <li>
          <a
            href="https://www.madebymike.com.au/writing/using-css-variables/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Utilising CSS variables
          </a>
        </li>

        <li>
          <a
            href="https://www.madebymike.com.au/writing/fluid-type-calc-examples/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Fluid Web Typography Examples
          </a>
        </li>

        <li>
          <a
            href="https://superfriendlydesign.systems/articles/typography-in-design-systems/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Typography in Design Systems
            <br />A typographic system that optimizes for guessability.{" "}
          </a>
        </li>

        <li>
          <a
            href="https://medium.com/sketch-app-sources/exploring-responsive-type-scales-cf1da541be54"
            target="_blank"
            rel="noopener noreferrer"
          >
            Exploring responsive type scales
          </a>
        </li>

        <li>
          <a
            href="https://www.freecodecamp.org/news/8-point-grid-typography-on-the-web-be5dc97db6bc/"
            target="_blank"
            rel="noopener noreferrer"
          >
            8-point grid typography on the web
          </a>
        </li>
      </ul>
    </div>
  );
};

export default References;
