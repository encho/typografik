import React from "react";
import styled from "styled-components";

import TypographySizes from "./TypographySizes";

const Typography = () => {
  return (
    <div>
      <h1>Typography</h1>

      <p>
        The resulting custom properties can be used to create design systems
        with css.
      </p>

      <h2>Base Text</h2>
      <p>
        Harmonious modular scales are used to create consistency throughout your
        web design. This creates harmony for users by diminishing or expanding
        key elements of your page — whether typography or clickable regions of a
        page — through a ratio. These gradual increases or decreases to your
        site’s elements, which are determined by mathematical computations
        (which we will look at later), create meaning and rhythm in your design.{" "}
      </p>

      <h2>Modular Scale</h2>

      <p>
        Harmonious modular scales are used to create consistency throughout your
        web design. This creates harmony for users by diminishing or expanding
        key elements of your page — whether typography or clickable regions of a
        page — through a ratio. These gradual increases or decreases to your
        site’s elements, which are determined by mathematical computations
        (which we will look at later), create meaning and rhythm in your design.{" "}
      </p>
      <p>
        The rhythm and consistency to your sites elements, created with the use
        of the modular scale, is far more time-efficient and aesthetically
        pleasing than working with arbitrary numbers pulled out of the air and
        then attributed to the elements.
      </p>

      <p>
        <a
          href="https://www.modularscale.com/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Calculate, visualize, and learn about modular scales
        </a>
      </p>

      <h2>Overview of the Modular Scale Sizes</h2>

      <p>
        <TypographySizes />
      </p>
    </div>
  );
};

export default Typography;
