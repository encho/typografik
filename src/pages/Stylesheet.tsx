import React from "react";
import css from "css";
import Highlight, { defaultProps } from "prism-react-renderer";
import theme from "prism-react-renderer/themes/oceanicNext";
import { CopyToClipboard } from "react-copy-to-clipboard";

const Stylesheet = () => {
  const cssStyles = Array.from(
    document.head.querySelectorAll("style[data-styled]")
  )
    .map(style => style.innerHTML)
    .join("");

  const parsedCSS = css.parse(cssStyles);

  let reachedGlobalCSS = false;

  parsedCSS.stylesheet.rules = parsedCSS.stylesheet.rules.reduce(
    (memo, rule, i) => {
      if (rule.type === "comment" && rule.comment.includes("sc-global")) {
        reachedGlobalCSS = true;
        rule.comment = `\n---------------------\nTypografik CSS v0.1.0\n---------------------\n`;
      }

      if (rule.type === "comment" && rule.comment.includes("sc-component-id")) {
        reachedGlobalCSS = false;
      }

      if (reachedGlobalCSS) {
        return [...memo, rule];
      }
      return [...memo];
    },
    []
  );

  const globalCSS = css.stringify(parsedCSS);

  return (
    <div>
      <h1>Stylesheet</h1>
      <p>
        To apply these typographic styles to your site, include the following
        CSS code into your web project.
      </p>
      <p>
        <CopyToClipboard text={globalCSS} onCopy={() => console.log("copied!")}>
          <button className="button">Copy CSS</button>
        </CopyToClipboard>
        <Highlight
          {...defaultProps}
          code={globalCSS}
          language="css"
          theme={theme}
        >
          {({ className, style, tokens, getLineProps, getTokenProps }) => {
            return (
              <pre
                className={className}
                style={{
                  ...style,
                  whiteSpace: "pre-wrap",
                  padding: "20px",
                  fontSize: "var(--modularScale__-2)"
                }}
              >
                {tokens.map((line, i) => (
                  <div {...getLineProps({ line, key: i })}>
                    {line.map((token, key) => (
                      <span {...getTokenProps({ token, key })} />
                    ))}
                  </div>
                ))}
              </pre>
            );
          }}
        </Highlight>
      </p>
    </div>
  );
};

export default Stylesheet;
