import React from "react";

const Home = () => {
  return (
    <div>
      <h1 style={{ fontSize: "var(--modularScale__5)" }}>Typografik</h1>
      <p style={{ fontSize: "var(--modularScale__1)", lineHeight: 1.2 }}>
        A project that helps you generate fluid CSS custom properties for more
        consistently styling your web typography and design systems projects.
      </p>
    </div>
  );
};

export default Home;
