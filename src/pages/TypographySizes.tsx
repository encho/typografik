import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
  background-color: var(--colors-primary);
  padding: calc(1 * var(--baseLine));
  overflow: scroll;
`;

const ModularScaleParagraph = ({ size }) => (
  <p
    style={{
      fontSize: `var(--modularScale__${size})`,
      lineHeight: `var(--modularScale__${size})`,
      whiteSpace: "nowrap",
      marginTop: 0,
      marginBottom: 0,
      paddingTop: 0,
      paddingBottom: 0
    }}
  >
    Modular scale {size}
  </p>
);

function TypographySizes() {
  return (
    <Wrapper>
      <ModularScaleParagraph size={-2} />
      <ModularScaleParagraph size={-1} />
      <ModularScaleParagraph size={0} />
      <ModularScaleParagraph size={1} />
      <ModularScaleParagraph size={2} />
      <ModularScaleParagraph size={3} />
      <ModularScaleParagraph size={4} />
      <ModularScaleParagraph size={5} />
      <ModularScaleParagraph size={6} />
      <ModularScaleParagraph size={7} />
    </Wrapper>
  );
}

export default TypographySizes;
