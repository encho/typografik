import React from "react";
import css from "css";
import Highlight, { defaultProps } from "prism-react-renderer";
import theme from "prism-react-renderer/themes/oceanicNext";
import { CopyToClipboard } from "react-copy-to-clipboard";

import useTypography from "../typography/useTypography";

const Settings = () => {
  const settings = useTypography();

  const settingsJSON = JSON.stringify(settings, undefined, 2);

  return (
    <div>
      <h1>Settings</h1>
      <p>Array of typographic settings in JSON format.</p>
      <p>
        <CopyToClipboard
          text={settingsJSON}
          onCopy={() => console.log("copied!")}
        >
          <button className="button">Copy JSON</button>
        </CopyToClipboard>
        <Highlight
          {...defaultProps}
          code={settingsJSON}
          language="json"
          theme={theme}
        >
          {({ className, style, tokens, getLineProps, getTokenProps }) => {
            return (
              <pre
                className={className}
                style={{
                  ...style,
                  whiteSpace: "pre-wrap",
                  padding: "20px",
                  fontSize: "var(--modularScale__-2)"
                }}
              >
                {tokens.map((line, i) => (
                  <div {...getLineProps({ line, key: i })}>
                    {line.map((token, key) => (
                      <span {...getTokenProps({ token, key })} />
                    ))}
                  </div>
                ))}
              </pre>
            );
          }}
        </Highlight>
      </p>
    </div>
  );
};

export default Settings;
